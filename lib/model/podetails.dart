import 'dart:async';

class POLine {
  final int poLineNum;
  final int poLineId;
  final String poLineDescription;
  final double poLineQuantity;
  final double poLineCost;
  final double poLineItemunitCost;
  final String poLineType;
  final String uom;
  final String workOrderNum;
  final String atachments;
  final String size;
  final String currencycode;

  const POLine({this.poLineNum,this.poLineId,this.poLineDescription,this.poLineQuantity,this.poLineCost,this.poLineItemunitCost,this.poLineType,this.uom,this.workOrderNum,this.currencycode,this.atachments,this.size});
 }

abstract class POLineRepository {
  Future<List<POLine>> fetchPOLineItems();
}

