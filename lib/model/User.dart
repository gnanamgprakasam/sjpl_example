import 'dart:async';
class Login {
  String username;
  String password;

  Login({this.username,this.password}); 

  Login.map(dynamic obj){
    this.username=obj['username'];
    this.password=obj['password'];
  }

  String get userid => username;
  String get pwd => password;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = username;
    map["password"] = password;
    return map;
  }
}

 
abstract class LoginRepository{
  ///Initialize and set up the login details.
  Future <List<Login>> login();

}

