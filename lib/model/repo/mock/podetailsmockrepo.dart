import 'dart:async';

import '../../podetails.dart';



class PoLineMockRepository implements POLineRepository {
  @override
  Future<List<POLine>> fetchPOLineItems() {
    // TODO: implement fetch
    return new Future.value(mockPOLine);
  }

}

var mockPOLine = <POLine>[
 new POLine(
 poLineDescription: "Contractor,Renewal Kit",
 poLineNum: 1,
 currencycode: "USD",
 poLineQuantity: 3.0,
 poLineCost: 327.00,
 poLineType: "material",
 atachments: "Electric Supplies_PO.pdf",
 size:"5.25 Mb"
),
const POLine(
 poLineDescription: "Building Thermostat",
 poLineNum: 2,
 poLineQuantity: 2.0,
 poLineCost: 148.00,
 poLineType: "material",
 currencycode: "USD",
 atachments: "Supplier Contract.pdf",
 size:"6.75 Mb"
),
new POLine(
 poLineDescription: "Electrical Motor",
  poLineQuantity: 3.0,
 poLineCost: 1148.00,
 poLineType: "material",
 currencycode: "USD",
 poLineNum: 3,
 atachments: "Approval Document.pdf",
 size:"7.75 Mb"
),


];
