import '../../di/di.dart';
import '../../model/User.dart';


/// Create an interface with two methods.
abstract class LoginContract{
  void onLoginSuccess(List<Login> login);
  void onLoginError();  
}
/// Create a class with two instances
class LoginPresenter{
  LoginContract _view;
  LoginRepository _repository;

  LoginPresenter(this._view){
    /// Assigning repository with the instance of loginRepository from Injector method.    
    _repository = new Injector().loginRepository;
  }

  void login(){
    /// Fecthing and passing values to the repository.
    assert(_view!=null);
    _repository
    .login()
    .then((d)=>_view.onLoginSuccess(d))
    .catchError((onError)=> _view.onLoginError());
  }
}
