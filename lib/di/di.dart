import '../model/User.dart';
import '../model/po.dart';
import '../model/podetails.dart';
import '../model/repo/mock/mockuserrepo.dart';
import '../model/repo/mock/podetailsmockrepo.dart';
import '../model/repo/mock/polistmockrepo.dart';
import '../model/repo/podetailsrepo.dart';
import '../model/repo/produserrepo.dart';


enum Flavor { MOCK,  PRO}
/// This is the Dependency Injection class 
class Injector {
  static final Injector _singleton= new Injector._internal();
  static Flavor _flavor;

  static void configure(Flavor flavor) {
    _flavor = flavor;
  }

  factory Injector() {
    return _singleton;
  }

Injector._internal();

PORepository get poRepo {
  switch(_flavor) {
    case Flavor.MOCK: return new PoListMockRepo();
    default: return new PoListMockRepo();

  }
}
 LoginRepository get loginRepository{
    switch(_flavor){
      case Flavor.MOCK:return new MockLoginRepository();
      default: return new ProdLoginRepository();      
    }
 }
 POLineRepository get poLineRepo{
   switch(_flavor){
     case Flavor.MOCK:return new PoLineMockRepository();
     default: return new ProdPoLineRepository();     
   }
 }
}