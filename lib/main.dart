import 'package:flutter/material.dart';

import 'di/di.dart';
import 'modules/login/loginwidget.dart';
import 'utils/routes.dart';



void main() async{
  Injector.configure(Flavor.MOCK);
  runApp(
    new MaterialApp(
      title: "TEAM",
      //home: new POListWidget(),
      home: new LoginPage(),
      routes: routes,
    )
  );
}