import 'package:flutter/material.dart';
import '../model/po.dart';
import '../model/podetails.dart';
import '../presenter/podetails/podetailspresenter.dart';
import '../model/repo/mock/polistmockrepo.dart';
import 'package:url_launcher/url_launcher.dart';



class PODetail extends StatefulWidget {
  PO podata=new PO();
  PODetail (PO po ) { 
    //key= super.key;
    this.podata=po;
    }

  @override
  _PODetailState createState() {
    // TODO: implement createState
    return new _PODetailState(podata);
  }
  
}

class _PODetailState extends State<PODetail> with TickerProviderStateMixin  implements POLineContract  {
  POLinePresenter _presenter;
  PO po;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  VoidCallback showBottomSheetCallBack;
  TabController tabController;
  
  
  List<POLine> _mockPOLine = new List();
  
    _PODetailState(PO podata){
   _presenter = new POLinePresenter(this);
   po=podata;
 }
 
 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _presenter.loadLineItems();
    showBottomSheetCallBack = showBottomSheet;
    tabController = new TabController(length: 3,vsync: this);
  }
  
  void showBottomSheet() {
      setState((){
        showBottomSheetCallBack = null;

      });
     scaffoldKey.currentState.showBottomSheet((BuildContext context) {
      final ThemeData themeData = Theme.of(context);  
        return new Container(
		decoration: new BoxDecoration(
          border: new Border(top: new BorderSide(color: themeData.disabledColor))
        ),
         
         child: new Row(
            children: <Widget>[
              new Container(
                 child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,	
                    children: <Widget>[
                      
                      new Container(
                        padding: const EdgeInsets.only(bottom:100.0,left:135.7),
                        alignment: Alignment.center,
                         child: new Text( "Attachments"),
                      ),
                      new Container(
                        padding: const EdgeInsets.only(bottom: 41.7),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset('asset/person_icon.png'),
                            new Padding(
                              padding: new EdgeInsets.only(left: 3.0),
                            child:new Text(
                            "PO - Detail.pdf",                            
                            textAlign: TextAlign.left,
                            softWrap: true,
                        )
                            )
                          ],
                        ),
                      )
                    ],
                 )

              )
            ],
          ),
        );
      }).closed.whenComplete((){
        if (mounted){
          setState((){
            showBottomSheetCallBack = showBottomSheet;
          });
        }
      });
  }
	@override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
   List<Widget> children = new List();
   children.add(_appBar());
   children.add(_buildCard()); 
   return new MaterialApp(
     
   home:  new Scaffold(
     
   backgroundColor:null ,
   body: new Stack( 
   children:children,
   
   ), 
   bottomNavigationBar: new Material(     
     child: new TabBar(
      indicatorColor : Colors.grey[50],
      controller: tabController,
       tabs: <Widget>[
         new Tab(                               
             icon: new IconButton(
               icon: new Icon(Icons.check,color: Colors.black,semanticLabel: "Accept",),
               onPressed: () {
                 return showDialog<Null>(
                  context: context,
                  barrierDismissible: false, 
                  builder: (BuildContext buildcontext) {
                  return new AlertDialog(
                   title: new Text("Accept Comments"),
                   content: new TextField(),
                   actions: <Widget>[
                     new FlatButton(
                       child: new Text("CANCEL"),
                       onPressed: () {
                         Navigator.of(buildcontext).pop();
                       },
                     ),
                     new FlatButton(
                       child: new Text("SEND"),
                       onPressed: () {
                         PoListMockRepo poListMockRepo=new PoListMockRepo();
                         int result=poListMockRepo.removePO(po.ponum);
                         if(result==1){
                           Navigator.of(buildcontext).pushReplacementNamed("/list");
                         }
                       },
                     ),
                   ],
                 );
               },
              );
               },
             ),
          ),
         new Tab(
               icon: new IconButton(
               icon: new Icon(Icons.clear,color: Colors.black,),
               onPressed: () {
                 return showDialog<Null>(
                  context: context,
                  barrierDismissible: false, 
                  builder: (BuildContext context) {
                  return new AlertDialog(
                   title: new Text("Reject Comments"),
                   content: 
                       new TextField(),
                     
                   actions: <Widget>[
                     new FlatButton(
                       child: new Text("CANCEL"),
                       onPressed: () {
                         Navigator.of(context).pop();
                       },
                     ),
                    new FlatButton(
                       child: new Text("SEND"),
                       onPressed: () {
                        PoListMockRepo poListMockRepo=new PoListMockRepo();
                         int result=poListMockRepo.removePO(po.ponum);
                         if(result==1){
                           Navigator.of(context).pushReplacementNamed("/list");
                         }
                       },
                     ), 
                   ],
                 );
               },
              );
               },
             ),

         ),
                  new Tab(
               icon: new IconButton(
               icon: new Icon(Icons.mail_outline,color: Colors.black,),
               onPressed: () {
                 launch("mailto:smith@example.org");

               },
             ),

         ),
                  new Tab(
               icon: new IconButton(
               icon: new Icon(Icons.call,color: Colors.black,),
               onPressed: () {
                 launch("tel://+91741894174");

               },
             ),
         ),
       ]
      ),
   ),
 ),
   );
   
  }
  Widget _appBar() =>
  
 new Scaffold(
   backgroundColor: null,
  key: scaffoldKey, 
    body:new Container(
    child: new Column( 
     crossAxisAlignment: CrossAxisAlignment.start,
     mainAxisSize:MainAxisSize.min,   
      children: <Widget>[        
        new CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          new SliverAppBar(
            backgroundColor:null ,
            pinned: true,
            elevation: 0.0,
            expandedHeight: 60.0, 
            title:const Text("PO - Detail"),
            flexibleSpace: const FlexibleSpaceBar(              
              background: const _AppBarBackground(),
            ),
            actions: <Widget>[
               new IconButton(
                padding: const EdgeInsets.only(right:27.0),
                icon: new Icon(Icons.attach_file),
                onPressed: () {showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                  return new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize:MainAxisSize.min,
          children: [
            new Container(
              padding: const EdgeInsets.only(left:135.7),
            child:new Text("Attachments", ),
            ),
          new Flexible(
              child: new ListView.builder(
                shrinkWrap: true,
                itemCount: _mockPOLine.length,
                itemBuilder: (BuildContext context, int index){
                 final POLine mockPOLineitems = _mockPOLine[index];
                 return _getattachmentslist(mockPOLineitems);   
                
                },
              ),
            ),
           ],
          
        ),

      );
                });
                },
                disabledColor: Colors.white,
                alignment: Alignment.centerRight,
              ),
            ],
          )
        ],
      ),
      
        new Container(
          color: null,
        child:new Image.asset("asset/appbar.png"),
        ),
        //new Image.asset("asset/appbar.png"),
        new Padding(
          padding: const EdgeInsets.only(top:15.0),
        ),
                new Flexible(
                  child: new ListView.builder(
                    shrinkWrap: true,
                    itemCount:_mockPOLine.length,
                    itemBuilder: (BuildContext context,int index){
                      print("inside list item");
                      final POLine mockPOLineitems = _mockPOLine[index];
                      return _getlistItemUi(mockPOLineitems);    
    
                    },
                  ),
                )
            
              ],
			        
              
           ),
 
 ),
  
 );
TextStyle headTextStyle =new TextStyle(
          fontFamily: 'Roboto-Regular',
          fontWeight: FontWeight.w600,
          fontSize: 15.2,);
          TextStyle subTextStyle =new TextStyle(
          fontFamily: 'Roboto-Regular',
          fontWeight: FontWeight.w400,
          fontSize: 11.2,
          color: Colors.grey
        );
        TextStyle dateTextStyle =new TextStyle(
          fontFamily: 'Roboto-Regular',
          fontWeight: FontWeight.w400,
          fontSize: 10.0,
          color: Colors.grey
    
        );
        TextStyle amountTextStyle =new TextStyle(
          fontFamily: 'Roboto-Regular',
          fontWeight: FontWeight.w400,
          fontSize: 21.0,
          color: Colors.blueAccent
        );
        
        TextStyle personTextStyle = new TextStyle(
          fontFamily: 'Roboto-Regular',
          fontWeight: FontWeight.w400,
          fontSize: 12.3,
          color: Colors.black
    
        );
  
  Widget _buildCard() {
  String priorityIcon;
    if (po.priority==1){
      priorityIcon='asset/highpriority_icon.png';
    }
    else if(po.priority==2){
      priorityIcon='asset/mediumpriority_icon.png';
    }
    else{
      priorityIcon='asset/lowpriority_icon.png';
    }
    String totalCost;
    if (po.currencycode == "USD") {
         totalCost = r'$ '+po.poTotalCost.toString();
    } else if (po.currencycode == "INR") {
         totalCost = "₹ "+po.poTotalCost.toString();
    }
  return new Padding(
          padding: const EdgeInsets.only(top:75.0,left:7.0,right:7.0,bottom:26.0),
            child: new Card(
          child: new Container(
            padding:const EdgeInsets.only(top: 14.3,left:14.0,bottom:12.7,right: 12.3),
            child: new Row(             
              children:[
                new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new Container(
                        alignment: Alignment.centerLeft,
                        child: new Text(
                          po.poDescription,
                          style: headTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),
                        
                      ),
                      new Container(
                        alignment: Alignment.centerLeft,
                        margin: const EdgeInsets.only(top: 8.3,bottom: 26.0) ,
                        child: new Text(
                          po.vendorName,
                          style: subTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),
                        
                      ),
                      new Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(left:2.0),
                        child:new Row(
                          children: [
                            new Image.asset('asset/person_icon.png'),
                            new Padding(
                              padding: new EdgeInsets.only(left: 3.0),
                            child:new Text(
                            po.requestBy,
                            style: personTextStyle,
                            textAlign: TextAlign.left,
                            softWrap: true,
                        )
                            )

                          ],
                        )                       
                      ),
                    ],
                  ),
                ),
                new Expanded(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      new Container(
                        alignment: Alignment.topRight,
                        margin: const EdgeInsets.only(left: 12.3,bottom: 16.3) ,
                        child: new Text(
                          po.reqDate,
                          style: dateTextStyle,
                          textAlign: TextAlign.left,
                          softWrap: true,
                        ),   
                      ),
                      new Container(
                        alignment: Alignment.centerRight,
                        margin: const EdgeInsets.only(bottom: 10.0),
                        child: new Text(
                          totalCost,
                          style: amountTextStyle,
                          textAlign: TextAlign.right,
                          softWrap: true,
                        ),   
                      ),
                      new Container(
                        alignment: Alignment.centerRight,
                        margin: const EdgeInsets.only(left:95.0,top:6.0),
                        child:new Row(
                          children: [
                            new Image.asset(priorityIcon),
                            new Padding(
                              padding: new EdgeInsets.only(left:3.0),
                            child:new Text(
                            po.ponum,
                            style: personTextStyle,
                            textAlign: TextAlign.right,
                            softWrap: true,
                        ),
                        )
                          ],
                        )   
                      )
                    ],
                  ),
                )
              ]
            )
          ),
        ),
			);
  }
       Widget _getlistItemUi(POLine mockPOLineitems){
       Widget listItemUiWidget;
       String number;
       String currency;
       Widget linetype;
       Widget polinetype;
         var mediaQuery = MediaQuery.of(context);
        
                     
         
       if (mockPOLineitems.poLineNum != null) {
          number = "#"+ mockPOLineitems.poLineNum.toString();
       } else {
          number = "";
       }
       if (mockPOLineitems.currencycode == "USD") {
         currency = r'$'+mockPOLineitems.poLineCost.toString();
       } else if (mockPOLineitems.currencycode == "Rupees") {
         currency = "₹"+mockPOLineitems.poLineCost.toString();
       }
    
       if (mockPOLineitems.poLineType == "material") {
         
         linetype = polinetype  ;
         
            } 
         
       
        TextStyle amounttextStyle =new TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w400,
          fontSize: 20.2,
          color: Colors.blue,
          letterSpacing: 0.02,
          
        );
        TextStyle descriptiontextStyle =new TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w400,
          fontSize: 15.2,
          color: Colors.black,
          letterSpacing: 0.02,
          
        );
        TextStyle quantitytextStyle =new TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w400,
          fontSize: 14.7,
          color: Colors.black,
          letterSpacing: 0.02,
          
        );
        TextStyle linenumtextStyle =new TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w400,
          fontSize: 14.7,
          color: Colors.grey,
          letterSpacing: 0.02,
          
        );
        TextStyle itemtypetextStyle =new TextStyle(
          fontFamily: 'Roboto',
          fontWeight: FontWeight.w400,
          fontSize: 10.7,
          color: Colors.black,
          letterSpacing: 0.02,
          
        );
        listItemUiWidget = 
          new Padding(
             padding: const EdgeInsets.only(top:8.0,left:7.0,right:7.0,bottom:10.0),
              child: new Card(
                elevation:1.0,
              child: new Container(        
              padding: const  EdgeInsets.only(right: 20.3,top:9.0,bottom:10.3,left:16.3),
              child: new Row(
              children:[
                new Expanded(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,				
                    children:[
                      new Container(
                        alignment: Alignment.centerLeft,
                         child: new Text( mockPOLineitems.poLineDescription,style:descriptiontextStyle ,),
                      ),
                       new Container(
                         child: new Row(
                          children: [
                          new Container(
                            alignment: Alignment.centerLeft,
                        margin: const EdgeInsets.only(top:12.3,bottom: 13.3,right:14.3),
                        child: new Text( mockPOLineitems.poLineQuantity.toString()+ " ""each", style: quantitytextStyle,),
                          ),
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,						  
                              children: [
                                new Container(
                                  padding: const EdgeInsets.only(top:12.3,bottom:13.3,),                                                     
                                  child: new Text(number,style: linenumtextStyle,),
                                 ),
                              ]
                            ),
                          ),
                          ],
                        ) 
                      ),
                      polinetype =
                      new Container(
                        alignment: Alignment.centerLeft,                    
                        padding: const EdgeInsets.only(left:2.0),
                        child:new Row(
                          children: <Widget>[
                          new Image.asset('asset/important_icon.png'),                                    
                         new Text( " "+"Non Standard iten",style: itemtypetextStyle,),
                          ],
                      ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  margin: const EdgeInsets.only(bottom:10.0 ),
                  alignment: Alignment.centerRight,
                 child: new Text(currency,style:amounttextStyle,)
                )
                
              ],
            ),
            
          )
         ),
       );
      return listItemUiWidget;     
      }
      Widget _getattachmentslist(POLine mockPOLineitems){
        Widget attachmentlistwidget;
       attachmentlistwidget =
       new Card(
         elevation: 0.5,
         
        child:new Container(
          padding: const EdgeInsets.only(left:16.7,top:19.7,bottom:7.0),
          child:new Row(          
            children: <Widget>[
              new Image.asset("asset/pdf_icon.png"),
              
              new Column(
                children: <Widget>[
                  new Container(  
              alignment: Alignment.centerLeft,                 
               margin: const EdgeInsets.only(left:16.3),
              child: new Text( mockPOLineitems.atachments,
              textAlign: TextAlign.left,
              ),
              ),
              new Text(mockPOLineitems.size,style: new TextStyle(
                       fontWeight: FontWeight.w200,
                       fontSize: 11.2,
                     ),),
                ],
              )

            ],
        
          ),


        )
      );
    return  attachmentlistwidget;
      }

      
         @override
      void onLoadPOLineComplete(List<POLine> items) {
        // TODO: implement onLoadPOLineComplete
        setState((){
         _mockPOLine = items;
    
        });
      }    
      @override
      void onLoadPOLineError() {
        // TODO: implement onLoadPOLineError
      }    
       
  }
     
  class _AppBarBackground extends StatelessWidget {
  const _AppBarBackground({Key key}) :super(key:key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Stack(      
   children: [
    new Positioned(
      child: new Image.asset(
        'asset/appbar.png',
        //alignment: Alignment.center,
      ),

    ),
      ]
    );
  }

}

