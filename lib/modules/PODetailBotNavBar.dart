import 'package:flutter/material.dart';

class NavigationIconView {
  NavigationIconView({
    Widget icon,
    Widget title,
    Color color,  
  }) : _icon = icon,
       _color = color,
       _title = title,
       item = new BottomNavigationBarItem(
         icon: icon,
         title: title,
         backgroundColor: color,
       );
  
  final Widget _icon;

  final Color _color;

  final Widget _title;

  final BottomNavigationBarItem item;
}
  
class BottomNavigation extends StatefulWidget {

  @override

  _BottomNavigationState createState() => new _BottomNavigationState();

}
class _BottomNavigationState extends State<BottomNavigation>{
int _currentIndex = 0;
List<NavigationIconView> _navigationViews;
@override
  void initState() {
    super.initState();
      _navigationViews = <NavigationIconView>[
      new NavigationIconView(
        icon: const Icon(Icons.check,color: Colors.black),
        title:const Text('Approve',),
       color: Colors.white,      
      ),
      new NavigationIconView(
        icon: const Icon(Icons.clear,color: Colors.black),
        title:const Text('Reject',),
        color: Colors.white,      
      ),
      new NavigationIconView(
        icon: const Icon(Icons.mail_outline,color: Colors.black),
        title:const Text('Mail',),
        color: Colors.white,      
      ),
      new NavigationIconView(
        icon: const Icon(Icons.call,color: Colors.black),
        title:const Text('Call',),
        color: Colors.white, 
      )
    ];
  }
  void _rebuild() {
    setState(() {
      // Rebuild in order to animate views.
    });
  }  
  @override
  Widget build(BuildContext context) {
     final BottomNavigationBar botNavBar = new BottomNavigationBar(
      items: _navigationViews
          .map((NavigationIconView navigationView) => navigationView.item)
          .toList(),
      currentIndex: _currentIndex, 
      onTap: (int i){setState((){_currentIndex = i;});},
    );
	return botNavBar;
	 }
}